# Copyright 2021, NVIDIA CORPORATION & AFFILIATES. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#  * Neither the name of NVIDIA CORPORATION nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
# OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

cmake_minimum_required(VERSION 3.17)

project(tutorialascendbackend LANGUAGES C CXX)

#
# Options
#
# Must include options required for this project as well as any
# projects included in this one by FetchContent.
#
# GPU support is disabled by default because recommended backend
# doesn't use GPUs.
#
option(TRITON_ENABLE_GPU "Enable GPU support in backend" OFF)
option(TRITON_ENABLE_STATS "Include statistics collections in backend" ON)
set(RapidJSON_DIR /data/gongen/rapidjson)
set(TRITON_COMMON_REPO_TAG "main" CACHE STRING "Tag for triton-inference-server/common repo")
set(TRITON_CORE_REPO_TAG "main" CACHE STRING "Tag for triton-inference-server/core repo")
set(TRITON_BACKEND_REPO_TAG "main" CACHE STRING "Tag for triton-inference-server/backend repo")

if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()
add_compile_options(-std=c++11)
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
# add_definitions(-D_GLIBCXX_USE_C99=1)
add_definitions(-D_GLIBCXX_USE_CXX11_ABI=0)
#
# Dependencies
#
# FetchContent requires us to include the transitive closure of all
# repos that we depend on so that we can override the tags.
#
include(FetchContent)

FetchContent_Declare(
  repo-common
  GIT_REPOSITORY https://gitee.com/Gongen/common.git
  GIT_TAG ${TRITON_COMMON_REPO_TAG}
  GIT_SHALLOW ON
)
FetchContent_Declare(
  repo-core
  GIT_REPOSITORY https://gitee.com/Gongen/core.git
  GIT_TAG ${TRITON_CORE_REPO_TAG}
  GIT_SHALLOW ON
)
FetchContent_Declare(
  repo-backend
  GIT_REPOSITORY https://gitee.com/Gongen/backend.git
  GIT_TAG ${TRITON_BACKEND_REPO_TAG}
  GIT_SHALLOW ON
)
FetchContent_MakeAvailable(repo-common repo-core repo-backend)

#
# The backend must be built into a shared library. Use an ldscript to
# hide all symbols except for the TRITONBACKEND API.
#
configure_file(src/libtriton_ascend.ldscript libtriton_ascend.ldscript COPYONLY)

set(MX_SDK_HOME $ENV{MX_SDK_HOME})
set(MXBASE_INC ${MX_SDK_HOME}/include)
set(MXBASE_LIB_DIR ${MX_SDK_HOME}/lib)
set(MXBASE_POST_LIB_DIR ${MX_SDK_HOME}/lib/modelpostprocessors)
set(MXBASE_POST_PROCESS_INC ${MX_SDK_HOME}/include/MxBase/postprocess/includecd )
set(OPENSOURCE_DIR ${MX_SDK_HOME}/opensource)

include_directories(${MXBASE_INC})
include_directories(${MXBASE_POST_PROCESS_INC})
include_directories(
  ${OPENSOURCE_DIR}/include
  ${OPENSOURCE_DIR}/include/opencv4
)

link_directories(${OPENSOURCE_DIR}/lib)
link_directories(${MXBASE_LIB_DIR})
link_directories(${MXBASE_POST_LIB_DIR})



add_library(
  triton-ascend-backend SHARED
  src/ascend.cc
  src/model_state.cc
  src/instance_state.cc
  src/ascend_util.cc
)

add_library(
  TutorialAscendBackend::triton-ascend-backend ALIAS triton-ascend-backend
)

target_include_directories(
  triton-ascend-backend
  PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}/src
)

target_compile_features(triton-ascend-backend PRIVATE cxx_std_11)
target_compile_options(
  triton-ascend-backend PRIVATE
  $<$<OR:$<CXX_COMPILER_ID:Clang>,$<CXX_COMPILER_ID:AppleClang>,$<CXX_COMPILER_ID:GNU>>:
    -Wall -Wextra -Wno-unused-parameter -Wno-type-limits -Werror>
  $<$<CXX_COMPILER_ID:MSVC>:/Wall /D_WIN32_WINNT=0x0A00 /EHsc>
)

target_link_libraries(
  triton-ascend-backend
  
  PRIVATE
    triton-core-serverapi   # from repo-core
    triton-core-backendapi  # from repo-core
    triton-core-serverstub  # from repo-core
    triton-backend-utils    # from repo-backend
    mxbase
    glog
    cpprest
    
)

# set_target_properties(
#   triton-ascend-backend
#   PROPERTIES
#     POSITION_INDEPENDENT_CODE OUTPUT_NAME
#     OUTPUT_NAME triton_ascend
#     SKIP_BUILD_RPATH TRUE
#     BUILD_WITH_INSTALL_RPATH TRUE
#     INSTALL_RPATH_USE_LINK_PATH FALSE
#     INSTALL_RPATH "$\{ORIGIN\}"
#     LINK_DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/libtriton_ascend.ldscript
#     LINK_FLAGS "-Wl,--version-script libtriton_ascend.ldscript"
# )

if(WIN32)
  set_target_properties(
    triton-ascend-backend PROPERTIES
    POSITION_INDEPENDENT_CODE ON
    OUTPUT_NAME triton_ascend
  )
else()
  set_target_properties(
    triton-ascend-backend PROPERTIES
    POSITION_INDEPENDENT_CODE ON
    OUTPUT_NAME triton_ascend
    LINK_DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/libtriton_ascend.ldscript
    LINK_FLAGS "-Wl,--version-script libtriton_ascend.ldscript"
  )
endif()

#
# Install
#
include(GNUInstallDirs)
set(INSTALL_CONFIGDIR ${CMAKE_INSTALL_LIBDIR}/cmake/TutorialAscendBackend)

install(
  TARGETS
    triton-ascend-backend
  EXPORT
    triton-ascend-backend-targets
  LIBRARY DESTINATION ${CMAKE_INSTALL_PREFIX}/backends/ascend
  RUNTIME DESTINATION ${CMAKE_INSTALL_PREFIX}/backends/ascend
)

install(
  EXPORT
    triton-ascend-backend-targets
  FILE
    TutorialAsendBackendTargets.cmake
  NAMESPACE
    TutorialAscendBackend::
  DESTINATION
    ${INSTALL_CONFIGDIR}
)

include(CMakePackageConfigHelpers)
configure_package_config_file(
  ${CMAKE_CURRENT_LIST_DIR}/cmake/TutorialAscendBackendConfig.cmake.in
  ${CMAKE_CURRENT_BINARY_DIR}/TutorialAscendBackendConfig.cmake
  INSTALL_DESTINATION ${INSTALL_CONFIGDIR}
)

install(
  FILES
  ${CMAKE_CURRENT_BINARY_DIR}/TutorialAscendBackendConfig.cmake
  DESTINATION ${INSTALL_CONFIGDIR}
)

#
# Export from build tree
#
export(
  EXPORT triton-ascend-backend-targets
  FILE ${CMAKE_CURRENT_BINARY_DIR}/TutorialAscendBackendTargets.cmake
  NAMESPACE TutorialAscendBackend::
)

export(PACKAGE TutorialAscendBackend)
