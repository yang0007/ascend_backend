#pragma once

#include <map>
#include <vector>

namespace triton { namespace backend { namespace ascend {

const uint32_t NCHW_WIDTH_INDEX = 3;
const uint32_t NCHW_HEIGHT_INDEX = 2;
const uint32_t NHWC_WIDTH_INDEX = 2;
const uint32_t NHWC_HEIGHT_INDEX = 1;

// 模型的动态类型
enum class DynamicType {
    STATIC_BATCH = 0,
    DYNAMIC_BATCH = 1,
    DYNAMIC_HW = 2,
    DYNAMIC_DIMS = 3,
    DYNAMIC_SHAPE = 4
};

enum class DataFormat {
    NCHW,
    NHWC,
    ND
};

struct ModelStrideInfo
{
    std::vector<uint32_t> min_input_gear {};
    std::vector<uint32_t> min_output_gear {};
    std::vector<uint32_t> max_input_gear {};
    std::vector<uint32_t> max_output_gear {};
};


// 模型的动态信息
struct DynamicInfo {
    DynamicType dynamic_type = DynamicType::STATIC_BATCH;
    std::vector<uint32_t> dynamic_batch {};
    std::vector<std::vector<uint32_t>> dynamic_size {};
    std::vector<std::vector<uint32_t>> dynamic_dims {};
};

bool HasDynamicShape(std::vector<std::vector<int64_t>>& data_shape);

bool CheckDataFormat(std::vector<DataFormat>& data_format);

}}};