#include "ascend_util.h"

namespace triton { namespace backend { namespace ascend {
bool HasDynamicShape(std::vector<std::vector<int64_t>>& data_shape)
{
    for (auto item : data_shape) {
        for (size_t i = 0; i < item.size(); i++) {
            if (item[i] == -1) {
                
                return true;
            }
        }
    }
    return false;
}

bool CheckDataFormat(std::vector<DataFormat>& data_format)
{
    for (size_t i = 0; i < data_format.size(); i++) {
        if (data_format[i] == DataFormat::ND) {
            return false;
        }
    }
    return true;
}

}}};